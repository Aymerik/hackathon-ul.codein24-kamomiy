'use strict';

const ConversationV1 = require('watson-developer-cloud/conversation/v1');

var express = require('express');
var app = express();
var port = process.env.PORT || 8000;
var bodyParser = require('body-parser');

var memory = {};

require('dotenv').config();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Using some globals for now
var conversation;
var redisClient;
var context;
var Wresponse;

function errorResponse(reason) {
    return {
        version: '1.0',
        response: {
            shouldEndSession: true,
            outputSpeech: {
                type: 'PlainText',
                text: reason || 'An unexpected error occurred. Please try again later.'
            }
        }
    };
}

function initClients() {
    return new Promise(function(resolve, reject) {
        // Connect a client to Watson Conversation
        conversation = new ConversationV1({
            password: process.env.WCS_Password,
            username: process.env.WCS_Username,
            version_date: '2016-09-20'
        });
        console.log('Connected to Watson Conversation');

        resolve("Done");
    });
}

function getJsonExcel() {
    var fs = require('fs');

    const excelToJson = require('convert-excel-to-json');

    const result = excelToJson({
        sourceFile: 'ADE-extract.ods'
    });

    var result2 = JSON.parse(JSON.stringify(result));
    //console.log(result2);

    for (var i = 1; i < result2.Sheet1.length; i++) {
        var heures = (result2["Sheet1"][i].D)*24;
        var minutes = (heures - Math.floor(heures))*60;
        result2["Sheet1"][i].D = "" + Math.floor(heures) + ":" + ((minutes == 0) ? "00" : minutes);
    }

    fs.writeFile("converted.json", JSON.stringify(result2), function(err) {
        if(err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });

    return result2;
}

function conversationMessage(request, workspaceId) {
    return new Promise(function(resolve, reject) {
        const input = request.inputs[0] ? request.inputs[0].rawInputs[0].query : 'start skill';
        var test = {
            input: { text: input },
            workspace_id: workspaceId,
            context: context
            //context: {}
        };
        console.log("Input" + JSON.stringify(test,null,2));
        conversation.message(
            {
                input: { text: input },
                workspace_id: workspaceId,
                context: context
            },
            function(err, watsonResponse) {
                if (err) {
                    console.error(err);
                    reject('Error talking to Watson.');
                } else {
                    console.log(watsonResponse);
                    context = watsonResponse.context; // Update global context

                    var reponsePerso = watsonResponse;
                    var result2 = getJsonExcel();

                    if (context.temps !== undefined) {
                        var temps = context.temps;
                        for (var i = 1; i < result2.Sheet1.length; i++) {
                            var date = new Date(temps);
                            var dateJSON = new Date(result2["Sheet1"][i].B);
                            //console.log(date, dateJSON);
                            if ((dateJSON).toDateString() == (date).toDateString()) {
                                console.log(date);
                                reponsePerso.output.text = [result2["Sheet1"][i].E + " avec " + result2["Sheet1"][i].G + "  en salle " + result2["Sheet1"][i].F];
                            }
                        }
                        resolve(reponsePerso);
                    } else {
                        resolve(watsonResponse);
                    }
                    //console.log(reponsePerso);
                    //resolve(reponsePerso);
                }
            }
        );
    });
}

function getSessionContext(sessionId) {
    console.log('sessionId: ' + sessionId);


    return new Promise(function(resolve, reject) {
        context = memory[sessionId];
        resolve();
    });
}

function saveSessionContext(sessionId) {
    console.log('---------');
    console.log('Begin saveSessionContext ' + sessionId);


    // Save the context in Redis. Can do this after resolve(response).
    if (context) {
        memory[sessionId] = context;
    }
}

function sendResponse(response, resolve) {
    // Combine the output messages into one message.ee
    var output = response.output.text.join(' ');
    //var quoi = context.cours;
    var result2 = getJsonExcel();
    response.output.text=("prout");
    console.log("affiche moi stp merci node js par pitiÃ© " + JSON.stringify(response));


    var resp = {
        conversationToken: null,
        expectUserResponse: true,
        expectedInputs: [
            {
                inputPrompt: {
                    richInitialPrompt: {
                        items: [
                            {
                                simpleResponse: {
                                    textToSpeech: output,
                                    displayText: output
                                }
                            }
                        ],
                        suggestions: []
                    }
                },
                possibleIntents: [
                    {
                        intent: 'actions.intent.TEXT'
                    }
                ]
            }
        ]
    };

    Wresponse =  resp;
    // Resolve the main promise now that we have our response
    resolve(resp);
}




app.post('/api/google4IBM', function(args, res) {
    return new Promise(function(resolve, reject) {
        const request = args.body;
        console.log("Google Home is calling");
        console.log(JSON.stringify(request,null,2));
        const sessionId = args.body.conversation.conversationId;
        initClients().then(() => getSessionContext(sessionId))
    .then(() => conversationMessage(request, process.env.workspace_id))
    .then(actionResponse => sendResponse(actionResponse, resolve))
    .then(data => {
            res.setHeader('Content-Type', 'application/json');
        res.append("Google-Assistant-API-Version", "v2");
        res.json(Wresponse);
    })
    .then(() => saveSessionContext(sessionId))
    .catch(function (err) {
            console.error('Erreur !');
            console.dir(err);
        });
    });
});

/*
	res.setHeader('Content-Type', 'application/json')
	res.append("Google-Assistant-API-Version", "v2")
*/
// start the server
app.listen(port);
console.log('Server started! At http://localhost:' + port);
